// pages/user/user.js
var _app = getApp()
Page({
  /**
   * 
   * 页面的初始数据
   */
  
  data: {
    
    menuitems: [
      { text: '我的优惠卷', url: '#', icon: '/images/youhuijuan.png', tips: '', arrows: '/images/myinfo1.png' },
      { text: '个人资料', url: '#', icon: '/images/user1.png', tips: '', arrows: '/images/myinfo1.png' },
      { text: '我的地址', url: '#', icon: '/images/user2.png', tips: '', arrows: '/images/myinfo1.png' },
      { text: '积分兑换', url: '#', icon: '/images/user3.png', tips: '', arrows: '/images/myinfo1.png' },
      { text: '更换号码', url: '#', icon: '/images/huanshoujihao.png', tips: '', arrows: '/images/myinfo1.png' },
      { text: '帮助说明', url: '#', icon: '/images/user4.png', tips: '', arrows: '/images/myinfo1.png' }
    ],
 imgurl:"https://img2.baidu.com/it/u=2998119432,2760702507&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=500",
nickname:'昵称'
    
  },
  
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  },
  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  },
  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  },
  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  },
  login:function () {
    var that=this;
    wx.getUserProfile({
      desc: '用于完善用户信息',
      success:function(res){
        console.log(res);
        var url=res.userInfo.avatarUrl;
        var nickname=res.userInfo.nickName;
        that.setData({
          imgurl:url,
          nickname:nickname
        })
      }
    })
  },
goyouhui:function(){
  wx.navigateTo({
    url: '../youhui/youhui',
  })
},
goyelist:function(){
  wx.navigateTo({
    url: '../yelist/yelist',
  })
},
gojifenlist:function(){
  wx.navigateTo({
    url: '../jifenlist/jifenlist',
  })
}
  
})
