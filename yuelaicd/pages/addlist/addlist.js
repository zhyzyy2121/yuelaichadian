
Page({
  data: {
    
    imgUrls: [
      '../../images/01.jpeg',
      '../../images/02.jpeg',
      '../../images/03.jpeg'
    ],
    indicatorDots: true,
    autoplay: true,
    interval: 5000,
    duration: 1000
  },
  onLoad: function () {
    
  },
  golist: function () {
    wx.switchTab({
      url: '../list/list'
    })
  },
  goindex: function () {
    wx.navigateTo({
   
      url: '../yylist/yylist'
    })
  },
})
