// pages/list/list.js
Page({
  data: {
    count:0,
    total:0,
    a:0,


    tablistx: ['活动商品', '新品推荐', '热销商品', '经典必喝', '限时折扣', '现磨咖啡', '清新原茶', '正经果茶', '限定套餐', '芝士奶盖', '原味冰激凌', '周边商品'],
    active: false,
    iNumy: 0,
    lists: [{
        arr: [{
            src: '/zhaopian/dnm.jpg',
            title: '冻冰蜜  ',
            jies: '入口新鲜，余味回甜',
            price:'￥8',
            num:1,
            id:1,
          },
          {
            src: '/zhaopian/nmxmqz.jpg',
            title: '柠檬小麦青汁',
            jies: '入口新鲜，余味回甜',
            price:'￥8',
            id:2,
            num:1,
          },
          {
            src: '/zhaopian/ttsmx.jpg',
            title: '桃桃思慕雪',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:3,
            num:1,
          },
          {
            src: '/zhaopian/zdtxgt.jpg',
            title: '真的甜西瓜桶',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:4,
            num:1,
          },
          {
            src: '/zhaopian/zdxnct.jpg',
            title: '真的甜奶茶桶',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:5,
            num:1,
          },

          {
            src: '/zhaopian/sdptn.jpg',
            title: '手捣葡萄柠',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:6,
            num:1,
          },
          {
            src: '/zhaopian/dnl.jpg',
            title: '冻柠绿',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:7,
            num:1,
          },







        ]
      },

      {
        arr: [{
            src: '/zhaopian/zylsxc.jpg',
            title: '真有料烧仙草',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:8,
            num:1,
          },
          {
            src: '/zhaopian/fmyzc.jpg',
            title: '蜂蜜柚子茶',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:9,
            num:1,
          },
          {
            src: '/zhaopian/bbpt.jpg',
            title: '冰雹葡萄',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:10,
            num:1,
          },
          {
            src: '/zhaopian/jjnm.jpg',
            title: '金桔柠檬',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:11,
            num:1,
          },
          {
            src: '/zhaopian/dnm.jpg',
            title: '冻柠蜜',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:12,
            num:1,
          },
        ]
      },


      {
        arr: [{
            src: '/zhaopian/mskf.jpg',
            title: '美式咖啡',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:13,
            num:1,
          },
          {
            src: '/zhaopian/bynt.jpg',
            title: '白椰拿铁',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:14,
            num:1,
          },
          {
            src: '/zhaopian/ybkf-jt.jpg',
            title: '益杯咖啡（焦糖）',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:15,
            num:1,
          },
          {
            src: '/zhaopian/ybkf-yw.jpg',
            title: '益杯咖啡（原味）',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:16,
            num:1,
          },
          {
            src: '/zhaopian/sywl.jpg',
            title: '丝韵乌龙',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:17,
            num:1,
          },
        ]
      },
      {
        arr: [{
            src: '/zhaopian/spnc.jpg',
            title: '双拼奶茶',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:18,
            num:1,
          },
          {
            src: '/zhaopian/lznc.jpg',
            title: '泷珠奶茶',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:19,
            num:1,
          },
          {
            src: '/zhaopian/hdnc.jpg',
            title: '红豆奶茶',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:20,
            num:1,
          },
          {
            src: '/zhaopian/bdnc.jpg',
            title: '布丁奶茶',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:21,
            num:1,
          },
          {
            src: '/zhaopian/bhnl.jpg',
            title: '薄荷奶绿',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:22,
            num:1,
          },
        ]
      },

      {
        arr: [{
            src: '/zhaopian/nmxmqz.jpg',
            title: '柠檬小麦青汁',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:23,
            num:1,
          },
          {
            src: '/zhaopian/mtwl.jpg',
            title: '蜜桃乌龙',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:24,
            num:1,
          },
          {
            src: '/zhaopian/yzgl.jpg',
            title: '杨枝甘露',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:25,
            num:1,
          },
          {
            src: '/zhaopian/ybbxg.jpg',
            title: '益杯百香果',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:26,
            num:1,
          },
          {
            src: '/zhaopian/xgymn.jpg',
            title: '西瓜燕麦奶',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:27,
            num:1,
          },
          {
            src: '/zhaopian/ddxxy.jpg',
            title: '冻冻小幸运',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:28,
            num:1,
          },
        ]
      },
      {
        arr: [{
            src: '/zhaopian/ttsmx.jpg',
            title: '桃桃思慕雪',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:29,
            num:1,
          },
          {
            src: '/zhaopian/hfnl.jpg',
            title: '禾风奶绿',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:30,
            num:1,
          },
          {
            src: '/zhaopian/ybsxc.jpg',
            title: '益杯烧仙草',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:31,
            num:1,
          },
          {
            src: '/zhaopian/yhkn.jpg',
            title: '益烤奶茶',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:32,
            num:1,
          },
          {
            src: '/zhaopian/ygnc.jpg',
            title: '椰果奶茶',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:33,
            num:1,
          },
          {
            src: '/zhaopian/htnl.jpg',
            title: '禾堂奶茶',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:34,
            num:1,
          },
        ]
      },
      {
        arr: [{
            src: '/zhaopian/hd.jpg',
            title: '红豆',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:35,
            num:1,
          },
          {
            src: '/zhaopian/xc.jpg',
            title: '仙草',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:36,
            num:1,
          },
          {
            src: '/zhaopian/ym.jpg',
            title: '燕麦',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:37,
            num:1,
          },
          {
            src: '/zhaopian/drjq.jpg',
            title: '多肉晶球',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:38,
            num:1,
          },
          {
            src: '/zhaopian/xmmz.jpg',
            title: '西米明珠',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:39,
            num:1,
          },
        ]
      },
      {
        arr: [{
            src: '/zhaopian/cfml.jpg',
            title: '翠峰茉莉',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:40,
            num:1,
          },
          {
            src: '/zhaopian/yhmd.jpg',
            title: '洋槐蜜冻',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:41,
            num:1,
          },
          {
            src: '/zhaopian/zz.jpg',
            title: '珍珠',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:42,
            num:1,
          },
          {
            src: '/zhaopian/yg.jpg',
            title: '椰果',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:43,
            num:1,
          },
          {
            src: '/zhaopian/bd.jpg',
            title: '布丁',
            jies: '入口新鲜，余味回甜，新鲜',
            price:'￥8',
            id:44,
            num:1,
          },
        ]
      },
    ],
     shopcar: [],
  },
show:function(){
  wx.navigateTo({
    url: '../yylist/yylist',
  })
},
    //  console.log(this.data.shopcar);
   
  close: function () {
    this.setData({
      active: false
    })
  },
  fntaby: function (e) {
    this.setData({
      iNumy: e.currentTarget.dataset.idx
    })
  },
  add(){
    this.setData({
      count:this.data.count+1,
    })
  },
  sub(){
    this.setData({
      count:this.data.count-1
    })
  },



  









  // add:function(res){
  //   var iNum=this.data.a;
  //   var idx=res.currentTarget.dataset.idx;
  //   var newArr=this.data.arr;
  //   iNum++;
  //   if(iNum<10){
  //     wx.showToast({
  //       title:'请确定你的订单',
  //       icon:'loading',
  //       duration:1000
  //     });
  //   }
  //   newArr[idx].num=iNum;
  //   this.setData({
  //     arr:newArr,
  //   })
  // }
})




