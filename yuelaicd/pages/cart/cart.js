// pages/cart/cart.js
Page({
  data: {
    isOk:false,
    count: 0,
    total: 0,
    isAllSelect: 'circle',
    status:0,
    arr: [{
        name: '订单一',
        price: 1000,
        isSelect: 'circle',
        imgUrl: 'https://img1.baidu.com/it/u=240229674,3191035678&fm=253&fmt=auto&app=138&f=JPEG?w=375&h=500',
        num: 1,
        id: 1
      },
      {
        name: '订单二',
        price: 2000,
        isSelect: 'circle',
        imgUrl: 'https://img1.baidu.com/it/u=1343483413,1443559859&fm=253&fmt=auto&app=138&f=JPEG?w=750&h=5004',
        num: 1,
        id: 1
      },
      {
        name: '订单三',
        price: 3000,
        isSelect: 'circle',
        imgUrl: 'https://img1.baidu.com/it/u=178868628,521494934&fm=253&fmt=auto&app=138&f=JPEG?w=500&h=750',
        num: 1,
        id: 1
      }
    ]
  },
  add:function(res){
    var iNum = res.currentTarget.dataset.number;
    var idx = res.currentTarget.dataset.idx;
    var newArr=this.data.arr;
  
    if(iNum<10){
      iNum++;
    }else{
      wx.showToast({
        title: '最多购买10件',
        icon:'loading',
        duration:1000
      });
      iNum=10;
    }
    newArr[idx].num=iNum;
    this.setData({
      arr:newArr,
    })
    this.total();
    this.price();
  },
  sub:function(arg){
    var iNum = arg.currentTarget.dataset.number;
    var idx = arg.currentTarget.dataset.idx;
    var newArr=this.data.arr;
    var that = this;
 
    if(iNum===1){
      wx.showToast({
        title: '商品将会被删除',
        duration: 500,
        icon:'loading',
      success:function(){
      that.setData({
        isOk:true
      })
         }
      })
if(this.data.isOk === true){
  newArr.splice(idx,1);
 this.setData({
  status:newArr.length
 })
}
    }else{
      iNum--;
      this.setData({
        isOk:false
      }) 
      newArr[idx].num=iNum;
    }
   
    this.setData({
      arr:newArr
    });
    this.total();
    this.price();
  },
  select:function(res){
    var newArr=this.data.arr
    var idx=res.currentTarget.dataset.idx;
    var sel=res.currentTarget.dataset.sel;
    var status=this.data.status;
if(sel==='circle'){
  var newtype='success';
  status++;
}else{
  var newtype='circle';
  status--;
}
if(status== newArr.length){
 this.setData({
  isAllSelect:'success'
 })
}else{
  this.setData({
    isAllSelect:'circle'
  })
}
newArr[idx].isSelect=newtype;
this.setData({
  arr:newArr,
  status
}) 
this.total();
this.price();
  },
  total:function(){
    var newArr=this.data.arr;
    var newNum=0;
    for(var i=0;i<newArr.length;i++){
      if(newArr[i].isSelect==='success'){
        newNum+=newArr[i].num;
      }
    }
    this.setData({
      total:newNum
    })
   
  },
  price:function(){
    var count=this.data.count;
    var newArr=this.data.arr;
    var iCount=0;
    for(var i=0;i<newArr.length;i++){
      if(newArr[i].isSelect==='success'){
        iCount+=newArr[i].num*newArr[i].price
      }
    }
this.setData({
  count:iCount
})

  },
  allSelect:function(reg){
  var sel=reg.currentTarget.dataset.sel;
  var newArr=this.data.arr;
  var status=this.data.status;
  if(sel==='circle'){
    for(var i=0;i<newArr.length;i++){
      newArr[i].isSelect='success';
    }
    this.setData({
      status:newArr.length
    })
var status='success';
  }else{
    for(var i=0;i<newArr.length;i++){
      newArr[i].isSelect='circle';
    }
    this.setData({
      status:0
    })
var status='circle';
  }
this.setData({
isAllSelect:status,
arr:newArr
});
this.total();
this.price();

  }
})